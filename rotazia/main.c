#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include "bmp.h"
#include "util.h"
#include "image.h"
#include <malloc.h>
#include "trans.h"


static const char* const writeMes[] = {//<-------------------FIXED!
    [WRITE_OK] = "Write is OK",
    [WRITE_ERROR] = "Hidden write problem",
    [WRITE_INVALID_SIGNATURE] = "Write with wrong args",
    [WRITE_INVALID_BITS] = "Pixel data problem with Write",
    [WRITE_INVALID_HEADER] = "Write met wrong Header",
    [WRITE_NULL_ERROR] = "Read met NULL",
    [W_OPEN_ERROR] = "Can't open to read",
    [NOT_FOUND1] = "Write not found" ,
        
};

static const char* const readMes[] = { //<-------------------FIXED!
    [READ_OK] = "Read is OK",
    [READ_INVALID_SIGNATURE] = "Read with wrong args",
    [READ_INVALID_BITS] = "Pixel data problem with Read",
    [READ_INVALID_HEADER] = "Read met wrong Header",
    [READ_NULL_ERROR] = "Read met NULL",
    [R_OPEN_ERROR] = "Can't open to read",
    [NOT_FOUND] = "Read not found" ,
};

const char *writeMesErr(enum read_status status) {
    return writeMes[status];
}

const char *readMesErr(enum write_status status) {
    return readMes[status];
}


void usage() {
    fprintf(stderr, "Usage: ./print_header BMP_FILE_NAME\n"); 
}

int main( int argc, char** argv ) {
    if (argc != 2) usage();
    if (argc < 2) err("Not enough arguments \n" );
    if (argc > 2) err("Too many arguments \n" );

    
    
    struct image img = { 0 };
    
    enum read_status read_status1 = from_bmp_file(argv[1], &img);
    
    if (read_status1==READ_OK){
    fprintf(stderr,  "It's OK - %s \n", readMesErr(read_status1));
    }
    else {
    err("Failed :%s \n", readMesErr(read_status1));
    }
    struct image rotateIm = rotate(&img);
    enum write_status write_status1 = to_bmp_file("damn.bmp", &rotateIm);
    if (write_status1==WRITE_OK){
        fprintf(stderr,  "It's OK - %s \n", readMesErr(write_status1));

    }
    else {
    err("Failed :%s \n", readMesErr(write_status1));
    }
        free(img.data);
        free(rotateIm.data);

    return 0;
}
