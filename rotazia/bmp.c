#include "bmp.h"
#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>
#include "image.h"
#include <stdlib.h>
#define BMP_TYPE 19778
#define BMP_SIZE (sizeof(struct bmp_header) - sizeof(uint16_t) - 3*sizeof(uint32_t))
#define PRI_SPECIFIER(e) (_Generic( (e), uint16_t : "%" PRIu16, uint32_t: "%" PRIu32, default: "NOT IMPLEMENTED" ))

#define PRINT_FIELD( t, name ) \
    fprintf( f, "%-17s: ",  # name ); \
    fprintf( f, PRI_SPECIFIER( header-> name ) , header-> name );\
    fprintf( f, "\n");

#define FOR_BMP_HEADER( FOR_FIELD ) \
        FOR_FIELD( uint16_t,bfType)\
        FOR_FIELD( uint32_t,bfileSize)\
        FOR_FIELD( uint32_t,bfReserved)\
        FOR_FIELD( uint32_t,bOffBits)\
        FOR_FIELD( uint32_t,biSize)\
        FOR_FIELD( uint32_t,biWidth)\
        FOR_FIELD( uint32_t,biHeight)\
        FOR_FIELD( uint16_t,biPlanes)\
        FOR_FIELD( uint16_t,biBitCount)\
        FOR_FIELD( uint32_t,biCompression)\
        FOR_FIELD( uint32_t,biSizeImage)\
        FOR_FIELD( uint32_t,biXPelsPerMeter)\
        FOR_FIELD( uint32_t,biYPelsPerMeter)\
        FOR_FIELD( uint32_t,biClrUsed)\
        FOR_FIELD( uint32_t,biClrImportant)

#define DECLARE_FIELD( t, n ) t n ; //<-------------------FIXED!

 struct __attribute__((packed)) bmp_header 
{
   FOR_BMP_HEADER( DECLARE_FIELD )
};



static bool read_header( FILE* f, struct bmp_header* header ) {
    return fread( header, sizeof( struct bmp_header ), 1, f );
}

static bool write_header( FILE* f, struct bmp_header *const header ) {
    return fwrite( header, sizeof( struct bmp_header ), 1, f );
}


int64_t padding(const uint32_t w){
    return w%4;
}





static enum read_status from_bmp( FILE* const in, struct image* const img){
    if (in==NULL) return READ_NULL_ERROR;

    struct bmp_header head={0};
    if (!read_header(in,&head)) return READ_INVALID_HEADER;
    img->height= head.biHeight;
    img->width=head.biWidth;
    fseek(in, sizeof(struct bmp_header), SEEK_SET);
    int64_t padding1 = padding(img->width);
    struct pixel* data = malloc(sizeof(struct pixel) * head.biHeight * head.biWidth);
    img->data = data;

    
    

    size_t bts = 0;
    for (size_t i=0; i<head.biHeight; i++){
        bts = bts + fread(&data[i*img->width], sizeof(struct pixel), head.biWidth, in );
        fseek(in, padding1, SEEK_CUR);

        
    }
    if (bts < head.biWidth ) return READ_INVALID_BITS;
    
    return READ_OK;
}
struct bmp_header create_bmp(struct image const* image){
    struct bmp_header header = {0};
header.bOffBits=sizeof(struct bmp_header);
header.bfReserved = 0;
header.bfType = BMP_TYPE;
header.bfileSize = sizeof(struct bmp_header) + sizeof(struct pixel)*image->width*image->height + image->height*(image->width%4);
header.biBitCount = sizeof(struct pixel)*8;
header.biPlanes = 1;
header.biHeight = image->height;
header.biSize = BMP_SIZE;
header.biSizeImage = header.bfileSize - header.bOffBits;
header.biWidth = image->width;
header.biBitCount = 24;
return header;
}

static enum write_status to_bmp( FILE* const out, const struct image* const img ){
    struct bmp_header head = create_bmp(img);
    if(!write_header(out, &head)) return WRITE_INVALID_HEADER;
    
    fseek(out, sizeof(struct bmp_header), SEEK_SET);

    const uint32_t height = img->height;
    const uint32_t width = img->width;
    struct pixel* datadata = img->data;
    int64_t padding1 = padding(img->width);
    size_t bts = 0;
    char buffer[] = { 'd' , 'a' , 'm', 'n' };
    
    
    for (size_t i=0; i<head.biHeight; i++){
        bts = bts + fwrite(datadata, sizeof(struct pixel)*width, 1, out);
        fwrite(buffer , sizeof(char), padding1, out);
        datadata= datadata + width;

        
    }
    if (bts < height) return WRITE_INVALID_BITS;
    
    return WRITE_OK;
}

enum read_status from_bmp_file( const char* const infile, struct image* const img){
    if (!infile) return NOT_FOUND;
    if (!img) return READ_INVALID_SIGNATURE;
    FILE* file = fopen(infile, "rb");
    if (!file) return R_OPEN_ERROR;
    enum read_status status = from_bmp(file, img);
    if (status){
        fclose(file);
        return status; 
    }
    fclose(file);
    return READ_OK;
}

enum write_status to_bmp_file(const char* const outfile , const struct image* const img ){
    if (!outfile) return NOT_FOUND;
    if (!img) return WRITE_INVALID_SIGNATURE;
    FILE* file = fopen(outfile, "wb+");
    if (!file) return W_OPEN_ERROR;
    enum write_status status = to_bmp(file, img);
    if (status){
        fclose(file);
        return status; 
    }
    return WRITE_OK;
    fclose(file);
}
