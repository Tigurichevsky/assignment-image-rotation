#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "image.h"





enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR,
    WRITE_INVALID_SIGNATURE,
    WRITE_INVALID_BITS,
    WRITE_INVALID_HEADER,
    WRITE_NULL_ERROR,
    W_OPEN_ERROR,
    W_SAVE_ERROR,
    NOT_FOUND
};


enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_NULL_ERROR,
    R_OPEN_ERROR,
    R_SAVE_ERROR,
    NOT_FOUND1
    };

enum read_status from_bmp_file( const char* filename, struct image* img);
enum write_status to_bmp_file(const char* filename , const struct image* img );

#endif
