
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#include "image.h"
#include "trans.h"

struct image rotate(struct image* const image){
    struct pixel* datadata = (struct pixel *) malloc(image->width*image->height * sizeof(struct pixel));
    struct image rotated = {.width = image->height, 
    .height=image->width,
    .data = datadata};
    for (uint64_t i=0; i< image->height; i++){
        for (uint64_t j=0; j< image->width; j++){
            rotated.data[(rotated.width*j + i)]= image->data[(image->width -1 -j)+ image->width*i]; 
        }
    }
    return rotated;
    
}
